package challenge;

import java.util.LinkedHashSet;
import java.util.Set;

public class Player {
    private String secretCode;

    public Player(boolean isNumericOpt) {
        this.secretCode = generateCode(isNumericOpt);
    }

    protected String generateCode(boolean isNumeric){
        return "";
    }

    public int[] compareCodeandGuess(String guessCode) {
        int bull = 0;
        int cow = 0;
        int[] guessResult = new int[2];

        for (int i = 0; i < 4; i++) {
            if (guessCode.charAt(i) == this.secretCode.charAt(i)) {
                bull++;
            } else {
                for (int j = 0; j < 4; j++) {
                    if (guessCode.charAt(i) == this.secretCode.charAt(j)) {
                        cow++;
                    }
                }
            }
        }

        if (bull == 4) {
            if (secretCode.equals(guessCode)) {
                System.out.println("You win!");
                System.exit(0);
            } else {
                System.out.println("You lose! :(");
                System.exit(0);
            }
        }
//        System.out.println("The correct code you need to guess is " + this.secretCode);
        System.out.println("Result: " + bull + " bull" + " and " + cow + " cow");

        // returning this for the smartAIPlayer to use
        guessResult[0] = bull;
        guessResult[1] = cow;
        return guessResult;
    }


}