package challenge;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Press 1 to play with letters. Press 2 to play with numbers. Press 3 to exit");
        final String numberEntered = input.nextLine();
        boolean isNumericOpt = false;

        if (numberEntered.equals("1")) {
            isNumericOpt = false;
        } else if (numberEntered.equals("2")) {
            isNumericOpt = true;
        } else if (numberEntered.equals("3")) {
            System.exit(0);
        } else {
            System.exit(0);
        }

        // Initialise two players, computer and human user
        Player aiPlayer = new AIPlayer(isNumericOpt);
        Player humanPlayer = new HumanPlayer(isNumericOpt);

        // Iterate through 7 turns of the game
        for (int i = 1; i < 8; i++) {
            // Human user's turn
            System.out.print("You guess: ");
            String guess = input.nextLine();
            // Computer (AI player) object will tell if the human guessed correctly against it's secret code
            Set<Character> guessCodeSet = new LinkedHashSet<>();
            for (int j = 0; j < guess.length(); j++) {
                guessCodeSet.add(guess.charAt(j));
            }

            boolean correctInput = true;

            if ((guess.length() != 4) || (guessCodeSet.size() != 4)) {
                System.out.println("Your guess code should be 4 characters long and contain no duplicates!");
                i--;
                correctInput = false;
            }
            else if (numberEntered.equals("2")){ //numbers
                if (!guess.matches("[0-9]+")){
                    System.out.println("You chose to play with integers, please enter an integer code");
                    i--;
                    correctInput = false;
                }
            }
            else if (numberEntered.equals("1")) {
                if (!guess.matches("[A-F]+")){
                    System.out.println("You chose to play with letters, please enter a code using uppercase letters A - F");
                    i--;
                    correctInput = false;
                }
            }

            if (correctInput == true){
                aiPlayer.compareCodeandGuess(guess);
                String computerGuess = aiPlayer.generateCode(isNumericOpt);

                // Computer's turn
                System.out.println("Computer guesses: " + computerGuess);
                humanPlayer.compareCodeandGuess(computerGuess);
                // Human player object will tell if the human guessed correctly against it's secret code
                System.out.println("round " + i + "/7 complete");
                input.nextLine();
            }

        }

        input.close();
        System.out.println("There was a tie!");
    }

}

