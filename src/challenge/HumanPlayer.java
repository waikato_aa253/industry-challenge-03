package challenge;

import java.util.*;
import java.io.*;

public class HumanPlayer extends Player {
    public HumanPlayer(boolean isNumericOpt) {
        super(isNumericOpt);
    }


    @Override
    protected String generateCode(boolean isNumeric) {
        Set<Character> playerCode = new LinkedHashSet<>();
        String playerCodeInput = "";
        String codeCharType;

        if (isNumeric == true) {
            codeCharType = "numbers";
        } else {
            codeCharType = "letters";
        }

        System.out.println("Please enter your secret code: ");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            playerCodeInput = reader.readLine();
        } catch (Exception e) {
            System.out.println(e);
        }

        for (int i = 0; i < playerCodeInput.length(); i++) {
            playerCode.add((playerCodeInput.charAt(i)));
        }

        if ((playerCodeInput.length() != 4) || (playerCode.size() != 4)) {
            System.out.println("Your secret code should be 4 characters long and contain no duplicates!");
            generateCode(isNumeric);
        }
        else if (isNumeric == true){ //numbers
            if (!playerCodeInput.matches("[0-9]+")){
                System.out.println("You chose to play with the integer game mode, please enter a integer code");
                generateCode(true);
            }
        }
        else if (isNumeric == false) {
            if (!playerCodeInput.matches("[A-F]+")){
                System.out.println("You chose to play with letters, please enter a code using uppercase letters A - F");
                generateCode(false);
            }
        }

        return playerCodeInput;

    }

}