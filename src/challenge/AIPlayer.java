package challenge;

import java.util.*;

public class AIPlayer extends Player{

    public AIPlayer(boolean isNumericOpt){
        super(isNumericOpt);
    }


    @Override
    protected String generateCode(boolean isNumeric) {
        String returnStr = "";
        int num;
        List<String> characterList;

        if (isNumeric == true) {
            characterList = new LinkedList<String>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));
            num = 10;
        } else {
            characterList = new LinkedList<String>(Arrays.asList("A", "B", "C", "D", "E", "F"));
            num = 6;
        }


        for (int i = 0; i < 4; i++) {
            Random rand = new Random();
            int randomInt = rand.nextInt(num);

            returnStr += characterList.get(randomInt);
            characterList.remove(randomInt);
            num--;
        }
        return returnStr;
    }
}